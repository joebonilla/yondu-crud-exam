<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::post('/user/store', [UserController::class, 'store'])->name('dashboard.store');
    Route::get('/user/{user}/edit', [UserController::class, 'show'])->name('dashboard.edit');
    Route::put('/user/{user}', [UserController::class, 'update'])->name('dashboard.update');
    Route::get('/dashboard', [UserController::class, 'index'])->name('dashboard.index');
    Route::get('/user/create', [UserController::class, 'create'])->name('dashboard.create');
    Route::put('/user', [UserController::class, 'destroy'])->name('dashboard.destroy');
});