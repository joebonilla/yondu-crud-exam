<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class UserTest extends TestCase
{

    use WithFaker;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testCreateUser()
    {
        $user = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'address' => $this->faker->address,
            'postcode' => $this->faker->postcode,
            'contact_phone_number' => $this->faker->phoneNumber,
            'email' => $this->faker->unique()->safeEmail,
            'username' => $this->faker->unique()->userName,
            'email_verified_at' => now(),
            'password' => bcrypt('password123'),
        ];

        $this->post(route('dashboard.store'), $user)->assertStatus(302);
    }

    public function testCanReadUserByID()
    {
        $this->get(route('dashboard.edit', 1))->assertStatus(302);
    }


    public function testCanReadUsersAll()
    {
        $this->get(route('dashboard.index'))->assertStatus(302);
    }


    public function testCanSearchUsersByUsername()
    {
        $user = [
            'username' => 'jmbonilla',
        ];

        $this->get(route('dashboard.index'), $user)->assertStatus(302);
    }

    public function testCanUpdateUser()
    {
        $user = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'address' => $this->faker->address,
            'postcode' => $this->faker->postcode,
            'contact_phone_number' => $this->faker->phoneNumber,
            'email' => $this->faker->unique()->safeEmail,
            'username' => $this->faker->unique()->userName,
            'email_verified_at' => now(),
        ];

        $this->put(route('dashboard.update', 1), $user)->assertStatus(302);
    }

    public function testCanReadDeleteUsers()
    {
        $this->put(route('dashboard.update', 1))->assertStatus(302);
    }
}
