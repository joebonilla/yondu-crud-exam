<?php
/**
 * Created by PhpStorm.
 * User: joemariebonilla
 * Date: 9/10/20
 * Time: 9:07 PM
 */

namespace App\Actions\Repo;


use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserQueries
{

    public function create($args)
    {
        User::create([
            'username' => $args['username'],
            'first_name' => $args['first_name'],
            'last_name' => $args['last_name'],
            'address' => $args['address'],
            'postcode' => $args['postcode'],
            'contact_phone_number' => $args['contact_phone_number'],
            'email' => $args['email'],
            'password' => Hash::make($args['password']),
        ]);
    }


    public function update($args, $model)
    {
        $model->update([
            'username' => $args['username'],
            'first_name' => $args['first_name'],
            'last_name' => $args['last_name'],
            'address' => $args['address'],
            'postcode' => $args['postcode'],
            'contact_phone_number' => $args['contact_phone_number'],
            'email' => $args['email'],
        ]);
    }

    public function destroy($args)
    {
        User::destroy($args->all());
    }

    public function filterByUsername($args)
    {
        return User::when($args->search_param, function ($query, $search_param) {
            $query->where('username', 'LIKE', '%' . $search_param . '%');
        })->orderBy('id', 'DESC')->paginate();
    }
}