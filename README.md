Exam purposes not intended to be perfect just to showcase the capabilities and standards.
Note 1: To edit user click the specific ID you want to edit.
Note 2: Intended to not put any validators/validation.

This exam is created in Laravel 8 Framrwork
Package: Jetstream, Inertia, VueJs, Tailwind CSS

Installation:

1. Create a database 
2. Copy and paste env.example to .env
3. Edit credentials in .env to your local DB
4. Run `composer install && npm install && npm run dev`
5. Run `php artisan migrate --seed`
6. Run `php artisan serve`